# Devops01

## Table of Contents

### Google Cloud  Basics
- Creating project 
- Assiging porject to other user
  
### Linux Basic 
- know Linux filesysystem
- Know Vi ,sed,less,tail

### Version Control using Git
- Clone 
- Pull
- push
- checkout
- branch
- pull request
- taging
- merging
- commit
### Configuration Management using Ansible 
- Installation of Ansible 
- Configuration
###  Docker
### Kubernetes
### Continuous Monitoring using
- Prometheus
- Grafana
- Alert Manager
### Continuous Logging
- Elasticsearch
- Kibana

### Build Tool
- Maven

### Static Code Analysis Tool
- Sonarqube

### Continous Integration , CD & CD 
- Jenkins 




